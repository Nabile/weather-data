﻿using System.Globalization;
using Sider;

namespace WeatherData.Extensions
{
    public static class RedisClientExtensions
    {
        private const char Separator = '|';

        public static WeatherInfo Get(this RedisClient client, string key)
        {
            string[] values = client.Get(key).Split(Separator);
            int index = 0;

            return new WeatherInfo
            {
                WindDirection = ushort.Parse(values[index++]),
                WindGust = float.Parse(values[index++]),
                WindSpeed = float.Parse(values[index++])
            };
        }

        public static void Set(this RedisClient client, string key, WeatherInfo value) => client.Set(key, string.Join(new string(Separator, 1), value.WindDirection, value.WindGust.ToString(CultureInfo.InvariantCulture), value.WindSpeed.ToString(CultureInfo.InvariantCulture)));
    }
}
