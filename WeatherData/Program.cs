﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using HtmlAgilityPack;
using Sider;

namespace WeatherData
{
    using Extensions;

    static class Program
    {
        const string LiveDataPath = "livedata.htm";

        static void Main(string[] args)
        {
            string stationAddress = "http://192.168.0.4";

            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case "--host":
                    case "-h":
                        stationAddress = "http://" + args[++i];
                        break;
                }
            }

            RedisClient client = new RedisClient();

            while (true)
            {
                try
                {
                    WebRequest request = WebRequest.Create(new Uri(new Uri(stationAddress), LiveDataPath));

                    using (Stream response = request.GetResponse().GetResponseStream())
                    {
                        // See: https://forums.asp.net/post/5574457.aspx
                        HtmlDocument document = new HtmlDocument();

                        document.Load(response);

                        WeatherValue type = WeatherValue.Unknown;
                        var entries = document.DocumentNode.Descendants().Where(n => n.Name == "input" && Enum.TryParse(n.GetAttributeValue("name", null), true, out type)).Select(n => new
                        {
                            Type = type,
                            Value = n.GetAttributeValue("value", null)
                        });

                        WeatherInfo info = new WeatherInfo();

                        foreach (var entry in entries)
                        {
                            switch (entry.Type)
                            {
                                case WeatherValue.WinDir:
                                    info.WindDirection = ushort.Parse(entry.Value, CultureInfo.InvariantCulture);
                                    break;
                                case WeatherValue.AvgWind:
                                    info.WindSpeed = float.Parse(entry.Value, CultureInfo.InvariantCulture);
                                    break;
                                case WeatherValue.GustSpeed:
                                    info.WindGust = float.Parse(entry.Value, CultureInfo.InvariantCulture);
                                    break;
                            }
                        }

                        DateTime date = DateTime.UtcNow;
                        string key = "weather:" + new DateTimeOffset(date).ToUnixTimeMilliseconds();

                        client.Set(key, info);
                        client.Expire(key, TimeSpan.FromDays(2.0));
                        Console.WriteLine($"[{date.ToLocalTime()}] {info}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[WARNING] Exception occurred while fetching data ({e.Message}).");
                }

                Thread.Sleep(TimeSpan.FromSeconds(30.0));
            }
        }
    }
}
