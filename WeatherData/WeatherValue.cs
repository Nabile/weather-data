﻿namespace WeatherData
{
    internal enum WeatherValue : byte
    {
        Unknown,
        WinDir,
        AvgWind,
        GustSpeed
    }
}
