﻿namespace WeatherData
{
    public struct WeatherInfo
    {
        #region Properties
        public ushort WindDirection { get; set; }

        public float WindSpeed { get; set; }

        public float WindGust { get; set; }
        #endregion

        #region Methods
        public override string ToString() => $"WeatherInfo: {{ WindDirection: {WindDirection}°, WindSpeed: {WindSpeed} km/h, WindGust: {WindGust} km/h }}";
        #endregion
    }
}
